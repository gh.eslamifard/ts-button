import styled from "@emotion/styled";

interface Props {
    done: boolean;
}

export const Wrapper = styled.div`
margin: auto;
width: 200px;
height: 200px;
display: flex;
align-items: center;
justify-content: center;
`;
export const Box = styled.div<Props>`
width: 50px;
height: 25px;
border-radius: 15px;
background-color:${({done})=>(done)?'#8aabff':'#e8e8e8'};
padding: 0 2px;
display: flex;
flex-direction: row;
align-items: center;
transition: 300ms;
`;
export const Circ =styled.div<Props>`
width: 20px;
height: 20px;
border-radius: 10px;
background-color: white;
margin-left: ${({done}) =>(done)?'29px':'0'};
transition: 300ms;
`;
export const Input = styled.input`
width: 0;
height: 0;
margin: 0;
`;