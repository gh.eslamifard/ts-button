import React from "react";
import {Box, Circ, Input, Wrapper} from "./style";

interface Props {
    value: boolean;
    onChange: (checked: boolean) => void;
}

export default function Button ({value,onChange}:Props) {
  // const [toggle,setToggle]=useState(false);
    const handler =()=>{
      //setToggle(!toggle)
        onChange(!value)
    };
    return(
        <Wrapper>
            <Box
                done={value}
                onClick={()=>{handler()}}
            >
                <Input type='checkbox' onChange={():void=>handler()} checked={value}/>
                <Circ done={value}/>
            </Box>
        </Wrapper>


    )
}