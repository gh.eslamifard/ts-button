import faker from 'faker';
import React from 'react';
import Button from "./index";
import {render,fireEvent} from '@testing-library/react';


// describe('Button',()=>{
//     it("renders without crashing", function () {
//         const {container} = render(<Button />);
//
//         expect(container.firstChild!.textContent).toBe('Test');
//     });
// });

describe('Button',()=>{
    it('render without crashing', function () {
        const buttonClick = jest.fn();
        const {container}= render(<Button value={false} onChange={buttonClick}/>);
        fireEvent.click(container.firstChild!.firstChild!);
        fireEvent.click(container.firstChild!.firstChild!);
        expect(buttonClick).toBeCalledTimes(2);
        expect(buttonClick).toBeCalledWith(true);
        expect(buttonClick).not.toBeCalledWith(false);
    })
});