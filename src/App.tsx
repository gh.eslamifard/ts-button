import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Button from "./comp/Button";

function App() {
    const[state,setState] = useState(false);
  return (
    <div className="App">
        {state ? 'true' : 'false'}
      <Button value={state} onChange={setState}/>
    </div>
  );
}

export default App;
